package mn.spock

@groovy.transform.MapConstructor
class Polygon {
    
    int numberOfSides
    Renderer renderer

    public Polygon(int numberOfSides) {
        if(numberOfSides <= 2){
            throw new TooFewSidesException("Dont create a polygon with $numberOfSides sides")
        }
        this.numberOfSides = numberOfSides
    }

    def draw() {
        (0..<numberOfSides).each {
            renderer.drawLine()
        }
    }
}