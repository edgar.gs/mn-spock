package mn.spock

import spock.lang.Specification
import spock.lang.Subject

public class ExampleSpec extends Specification{

    @Subject
    def polygon = new Polygon(5)
    
    def "simple assertion"() {
        expect:
        1 == 1
    }

    def "demostrate given-when-then"() {
        given:
        def polygon = new Polygon(4)

        when:
        int sides = polygon.numberOfSides

        then:
        sides == 4
    }

    def "demostrate given-when-then subject object"() {
        when:
        int sides = polygon.numberOfSides

        then:
        sides == 5
    }

    def "should expect Exceptions"() {
        when:
        new Polygon(0)

        then:
        def e = thrown(TooFewSidesException)
        e.getMessage() == "Dont create a polygon with 0 sides"
    }

    def "should expect an Exception to be thrown for a number of invalid inputs"() {
        when:
        new Polygon(sides)

        then:
        def e = thrown(TooFewSidesException)
        e.getMessage() == "Dont create a polygon with $sides sides"

        where:
        sides << [0,1,2]
    }

    def "should be able to create a polygon with #sides sides"() {
        expect:
        new Polygon(sides).numberOfSides == sides

        where:
        sides << [3,8,13,55]
    }

    def "should be able to mock a concret class"() {
        given:
        def mockRenderer = Mock(Renderer)
        @Subject
        def polygon = new Polygon(numberOfSides:4, renderer:mockRenderer)

        when:
        polygon.draw()

        then:
        4 * mockRenderer.drawLine()
    }

    def "should be able to create a stub"() {
        given:
        def stubPalette = Stub(Palette)
        stubPalette.getPrimaryColour() >> Colour.RED
        @Subject
        def renderer = new Renderer(stubPalette)

        expect:
        renderer.getForegroundColour() == Colour.RED
    }

    def "should use a helper method"() {
        given:
        Renderer mockRenderer = Mock()
        def shapeFactory = new ShapeFactory(mockRenderer)

        when:
        def polygon = shapeFactory.createDefaultPolygon()

        then:
        checkDefaultShape(polygon, mockRenderer)        
    }

    private void checkDefaultShape(Polygon polygon, Renderer renderer) {
        assert polygon.numberOfSides == 4
        assert polygon.renderer == renderer
    }

    def "should use verifyAll"() {
        given:
        Renderer mockRenderer = Mock()
        def shapeFactory = new ShapeFactory(mockRenderer)

        when:
        def polygon = shapeFactory.createDefaultPolygon()

        then:
        verifyAll(polygon) {
            numberOfSides == 4
            renderer == renderer
        }
    }
}
