package mn.spock

@groovy.transform.TupleConstructor
class ShapeFactory {
    Renderer renderer

    /*public ShapeFactory(Renderer renderer) {
        this.renderer = renderer
    }*/

    public Polygon createDefaultPolygon() {
        return new Polygon(numberOfSides: 4, renderer: renderer)
    }
}