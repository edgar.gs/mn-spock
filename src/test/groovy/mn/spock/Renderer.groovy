package mn.spock

@groovy.transform.TupleConstructor
class Renderer {

    Palette palette

    def drawLine() {}

    def getForegroundColour() {
        palette.getPrimaryColour()
    }
}