package mn.spock

class TooFewSidesException extends IllegalArgumentException {

    TooFewSidesException(message) {
        super(message)
    }
}
